FROM python:latest

RUN mkdir /btc-app
COPY python /btc-app/
RUN pip install /btc-app/

ENTRYPOINT ["python3", "-m", "process_btc"]
