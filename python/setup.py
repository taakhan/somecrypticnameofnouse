import os
import setuptools
from distutils.core import setup

setup(
    name='process_btc',
    version='0.0.1',
    packages=setuptools.find_packages(),
    url='',
    license='',
    author='tahakhan',
    author_email='',
    description='bitcoin price processing data pipeline.',
    install_requires=[
              'pandas==0.25.3',
              'matplotlib==3.1.2',
              'argparse==1.4.0'
          ],
    include_package_data=True
)
