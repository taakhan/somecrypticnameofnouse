import argparse
import os

import process_btc.lib
from process_btc.lib.pipeline import make_results


def parse_arguments():
    parser = argparse.ArgumentParser()
    code_path = os.path.dirname(os.path.realpath(__file__))
    parser.add_argument('--input-file-path',
                        dest='input_file_path', default=code_path+'/btc.csv')
    parser.add_argument('--output-directory',
                        dest='output_directory', default='./summary')
    args = parser.parse_args()

    dict_args_values = {}

    dict_args_values['input_file_path'] = args.input_file_path
    dict_args_values['output_directory'] = args.output_directory

    return dict_args_values


def main():
    dict_args_values = parse_arguments()

    input_file_path = dict_args_values.get('input_file_path')
    output_directory = dict_args_values.get('output_directory')

    make_results(
        input_file_path,
        output_directory,
        0.87,
        'date',
        ['marketcap(USD)', 'price(USD)']
    )


if __name__ == '__main__':
    main()
