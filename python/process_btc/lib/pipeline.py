import datetime
import re
from datetime import datetime, timedelta
from pathlib import Path

import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters

import matplotlib.pyplot as plt


def create_directory_ifnotexists(path):
    """Safely creates a directory with parents.

    Arguments:
        path {string} -- Full directory path.
    """
    Path(path).mkdir(parents=True, exist_ok=True)


def read_csv_return_df(path):
    """Reads and converts a CSV file in a Pandas Dataframe.

    Arguments:
        path {string} -- CSV File path to read

    Returns:
        pandas.Dataframe -- Dataframe from CSV
    """
    return pd.read_csv(path)


def discard_not_needed_columns(df):
    """Takes a dataframe, strips additional columns, returns the dataframe.

    Arguments:
        df {pd.Dataframe} -- Input Dataframe

    Returns:
        pd.Dataframe -- Dataframe with only columns of interest.
    """
    return df[['date', 'generatedCoins', 'paymentCount', 'marketcap(EUR)', 'price(EUR)']]


def convert_pdseries_to_datetime(pd_series):
    """Converts a Pandas Series values to datetime object.

    Arguments:
        pd_series {pandas.Series} -- Input series to be converted to datetime object

    Returns:
        pd.Series -- Converted datetime series.
    """
    return pd.to_datetime(pd_series)


def filter_last_365_days(df, date_column_name):
    """Takes a dataframe and a date column name.
    Filters the dataframe to retain values with N-365 date;
    where N is maximum value of the passed column name.

    Arguments:
        df {pd.Dataframe} -- Input Dataframe
        date_column_name {string} -- Column name to filter on

    Returns:
        pd.Dataframe -- Filtered Dataframe
    """
    df[date_column_name] = convert_pdseries_to_datetime(df[date_column_name])
    timedelta_365days = timedelta(days=365)
    return df[df[date_column_name] >= df[date_column_name].max() - timedelta_365days]


def convert_usd_to_eur_return_pdseries(pd_series, conversion_rate):
    """Takes a Pandas series (assumes it to represent Monetary value in numbers in USD).
    Takes a Conversion Rate for USD to EUR conversion.
    Converts the Monetary amount according to conversion rate.

    Arguments:
        pd_series {pd.Series} -- Series containing monetary values in USD
        conversion_rate {[type]} -- USD to EUR Conversion Rate

    Returns:
        ps.Series -- Currency converted Monetary Values.
    """
    return pd_series * conversion_rate


def add_currency_converted_columns_to_df(df, source_target_currency_column_names_dict, conversion_rate):
    """Converts given columns in a given Dataframe from USD values to EUR values,
    creates new columns for converted values.

    Arguments:
        df {pd.Dataframe} -- Dataframe containing USD columns
        source_target_currency_column_names_dict {dict} -- A Dictionary of USD column names and their Target EUR column Names
        conversion_rate {number} -- USD to EUR conversion rate

    Returns:
        pd.Dataframe -- a Dataframe with USD columns converted, and new values saved in new columns.
    """
    for usd_currency_column, eur_currency_column in source_target_currency_column_names_dict.items():
        df[eur_currency_column] = convert_usd_to_eur_return_pdseries(
            df[usd_currency_column], conversion_rate)
    return df


def take_df_write_csv(df, output_path):
    """Takes a dataframe, and a path, and writes CSV.

    Arguments:
        df {pd.Dataframe} -- Dataframe to write as CSV
        output_path {string} -- Path where CSV file should be written
    """
    create_directory_ifnotexists(output_path)
    df.to_csv(output_path + '/summary.csv')


def summarize_and_transpose_data(df, summarize_column_names):
    """For a dataframe, calculates min, max and average for the given columns.

    Arguments:
        df {pd.Dataframe} -- Dataframe to be summarized
        summarize_column_names {List[String]} -- Columns to summarize in the Dataframe

    Returns:
        pd.Dataframe -- Summary Dataframe
    """
    list_operations = ["min", "max", "mean"]
    df = df[summarize_column_names].agg(list_operations).transpose()
    df = df.rename(columns={'mean': 'average'})
    return df


def calculate_series_sum(pd_series):
    """Calculate a sum for Pandas Series.

    Arguments:
        pd_series {pd.Series} -- Series to sum

    Returns:
        number -- Result of summing a series
    """
    return pd_series.sum()


def get_source_target_currency_column_dict(source_currency_columns):
    """Takes a list of column names with assumingly the string USD in them.
    Replaces the USD with EUR and returns a source -> target conversion dictionary.

    Arguments:
        source_currency_columns {List[String]} -- List of column names with substring "USD"

    Returns:
        Dict -- Dictionary with keys as strings in argument list,
        and values with same string with USD substituted with EUR.
    """
    source_target_names_dict = {}
    for usd_currency_column in source_currency_columns:
        pattern = re.compile("usd", re.IGNORECASE)
        eur_currency_column = pattern.sub('EUR', usd_currency_column)
        source_target_names_dict[usd_currency_column] = eur_currency_column
    return source_target_names_dict


def export_price_trend(df, output_path, xaxis_column, yaxis_column, x_label, y_label, title):
    """Takes a Dataframe, plots a line chart based on the xaxis_column and yaxis_column,
    exports the result as a png file.

    Arguments:
        df {pd.Dataframe} -- Dataframe with axes to plot
        output_path {String} -- Path to save the exported file
        xaxis_column {String} -- column to plot on x_axis
        yaxis_column {String} -- column to plot on y_axis
    """
    register_matplotlib_converters()
    df = df.sort_values('date', ascending=True)
    plt.title(title)
    plt.xlabel('Date (EUR)')
    plt.ylabel('Price (EUR)')
    plt.xticks(rotation='vertical')
    plt.plot(df[xaxis_column], df[yaxis_column])
    create_directory_ifnotexists(output_path)
    plt.savefig(output_path + '/line_chart.png', bbox_inches="tight")


def make_results(input_path, output_path, conversion_rate, date_column_name, usd_currency_columns):
    df = read_csv_return_df(input_path)

    source_target_currency_column_names_dict = get_source_target_currency_column_dict(
        usd_currency_columns)
    euro_currency_columns_names = [
        v for k, v in source_target_currency_column_names_dict.items()]

    df = filter_last_365_days(df, date_column_name)
    df = add_currency_converted_columns_to_df(
        df, source_target_currency_column_names_dict, conversion_rate)
    df = discard_not_needed_columns(df)

    take_df_write_csv(df, output_path)

    total_coins = calculate_series_sum(df['generatedCoins'])
    total_coins_string = f"Total Bitcoins generated in last year: {total_coins}"
    print(total_coins_string)

    summarize_column_names = euro_currency_columns_names + \
        ['generatedCoins', 'paymentCount']
    df_transpose = summarize_and_transpose_data(df, summarize_column_names)
    print("Metrics Summary:")
    print(df_transpose)

    export_price_trend(df, output_path, 'date', 'price(EUR)',
                       'Date', 'Price (EUR)', 'Bitcoin Price in EUR over time')
