# Bitcoin Processing App

## Description
A python app that processes Bitcoin pricing and market data.
It does the following:
    1. Reads a dataset in CSV (including daily price and market statistics for bitcoin).
    2. Filters for the Last 365 days, starting from the maximum date in the dataset.
    3. Converts USD currency values to EUR currency values assuming a conversion rate of 0.87.
    4. Cleans unwanted columns .
    5. Writes the file to an output location.
    6. Creates and prints a summary of some metrics on the standard output.
    7. Creates a line chart displaying a trend of bitcoin prices

## Installation

**(tested) Python Version: 3.8.1**

The application can be installed via the following command from the `python` directory:

    pip install .

## Execution
Once installed, the application can be executed with the following command:

```
python3 -m process_btc
```

When executed without any parameters, the application takes the sample data file provided with the technical case, and produces output in a new folder named `summary` in execution location.

The application supports parameters which can be listed by:

```
python3 -m process_btc --help
```

#### Output:

```
usage: __main__.py [-h] [--input-file-path INPUT_FILE_PATH]
                   [--output-directory OUTPUT_DIRECTORY]

optional arguments:
-h, --help            show this help message and exit
--input-file-path INPUT_FILE_PATH
--output-directory OUTPUT_DIRECTORY
```

### Example Usage:
If my data file resides in `/path/to/datafile.csv` and I wish to receive the output in `/output`, I would execute:

```
python3 -m process_btc --input-file-path /path/to/datafile.csv --output-directory /output
```

## Dockerized Execution

### Additionally, a Docker image can be optionally built with the application by executing the following from the project root:


```
docker build -t <desired image name> .
```

### Execute the app:


```
docker run <desired image name> [optional application parameters (see above)]
```

(Hint: To receive output, you might want to mount a host path to container and pass the `--output-directory` parameter to the application when running the container. )


If you do not want to build the image, an already built image can be found [here](https://hub.docker.com/repository/docker/dockerdot/somecrypticnameofnocommonuse).


## Author Information:
For any questions or queries, please free to reach out to: `m.taha.khan@hotmail.com`
